using NUnit.Framework;
using TestPractica;
using Practica_T1;

namespace TestPractica.Test
{
    public class Tests
    {

        private NumeroRomano Prueba = new NumeroRomano();

        [Test]
        public void Test1()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(359);
            Assert.AreEqual("CCCLIX",prueba);
        }
        [Test]
        public void Test2()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(15);
            Assert.AreEqual("XV", prueba);
        }
        [Test]
        public void Test3()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(151);
            Assert.AreEqual("CLI", prueba);
        }
        [Test]
        public void Test4()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(999);
            Assert.AreEqual("CMXCIX", prueba);
        }
        [Test]
        public void Test5()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(1000);
            Assert.AreEqual("M", prueba);
        }
        [Test]
        public void Test6()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(995);
            Assert.AreEqual("CMXCV", prueba);
        }
        [Test]
        public void Test7()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(775);
            Assert.AreEqual("DCCLXXV", prueba);
        }
        [Test]
        public void Test8()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(77);
            Assert.AreEqual("LXXVII", prueba);
        }
        [Test]
        public void Test9()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(393);
            Assert.AreEqual("CCCXCIII", prueba);
        }
        [Test]
        public void Test10()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(710);
            Assert.AreEqual("DCCX", prueba);
        }
        [Test]
        public void Test11()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(500);
            Assert.AreEqual("D", prueba);
        }
        [Test]
        public void Test12()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(410);
            Assert.AreEqual("CDX", prueba);
        }
        [Test]
        public void Test13()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(618);
            Assert.AreEqual("DCXVIII", prueba);
        }
        [Test]
        public void Test14()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(697);
            Assert.AreEqual("DCXCVII", prueba);
        }
        [Test]
        public void Test15()
        {
            string prueba = "";
            prueba = Prueba.GetNumeroRomano(205);
            Assert.AreEqual("CCV", prueba);
        }
    }
}