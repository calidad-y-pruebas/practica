﻿using NUnit.Framework;
using TestPractica;
using Practica_T1;

namespace TestPractica.Test
{
    public class PiedraPapelTijeraTest
    {
        private PiedraPapelTijera juego = new PiedraPapelTijera();
        private Jugador jg1 = new Jugador();
        private Jugador jg2 = new Jugador();

        [Test]
        public void Test1()
        {
            string result = "";
            jg1.Nombre = "Pablo";
            jg1.Eleccion = "Tijera";

            jg2.Nombre = "Sthefanie";
            jg2.Eleccion = "Papel";
            result = juego.GetGanador(jg1,jg2);

            Assert.AreEqual("Pablo", result);
        }
        [Test]
        public void Test2()
        {
            string result = "";
            jg1.Nombre = "Liliana";
            jg1.Eleccion = "Papel";

            jg2.Nombre = "Jhoana";
            jg2.Eleccion = "Piedra";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Liliana", result);
        }
        [Test]
        public void Test3()
        {
            string result = "";
            jg1.Nombre = "Antonio";
            jg1.Eleccion = "TijErA";

            jg2.Nombre = "Nadia";
            jg2.Eleccion = "PaPel";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Antonio", result);
        }
        [Test]
        public void Test4()
        {
            string result = "";
            jg1.Nombre = "Carmen";
            jg1.Eleccion = "piedra";

            jg2.Nombre = "Katy";
            jg2.Eleccion = "TijerA";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Carmen", result);
        }
        [Test]
        public void Test5()
        {
            string result = "";
            jg1.Nombre = "Thalia";
            jg1.Eleccion = "Tijera";

            jg2.Nombre = "Sharong";
            jg2.Eleccion = "TiJera";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("EMPATE", result);
        }
        [Test]
        public void Test7()
        {
            string result = "";
            jg1.Nombre = "Ana";
            jg1.Eleccion = "Papel";

            jg2.Nombre = "Karolyn";
            jg2.Eleccion = "Papel";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("EMPATE", result);
        }
        [Test]
        public void Test8()
        {
            string result = "";
            jg1.Nombre = "Piero";
            jg1.Eleccion = "Papel";

            jg2.Nombre = "Claudia";
            jg2.Eleccion = "Tijera";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Claudia", result);
        }
        [Test]
        public void Test9()
        {
            string result = "";
            jg1.Nombre = "Nelson";
            jg1.Eleccion = "Tijera";

            jg2.Nombre = "Oswaldo";
            jg2.Eleccion = "Papel";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Nelson", result);
        }
        [Test]
        public void Test10()
        {
            string result = "";
            jg1.Nombre = "Pablo";
            jg1.Eleccion = "piedra";

            jg2.Nombre = "Sthefanie";
            jg2.Eleccion = "Papel";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Sthefanie", result);
        }
        [Test]
        public void Test11()
        {
            string result = "";
            jg1.Nombre = "Cristian";
            jg1.Eleccion = "Tijera";

            jg2.Nombre = "Natalie";
            jg2.Eleccion = "Piedra";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Natalie", result);
        }
        [Test]
        public void Test12()
        {
            string result = "";
            jg1.Nombre = "Luis";
            jg1.Eleccion = "piedra";

            jg2.Nombre = "Patricia";
            jg2.Eleccion = "PIEDRA";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("EMPATE", result);
        }
        [Test]
        public void Test13()
        {
            string result = "";
            jg1.Nombre = "Juan";
            jg1.Eleccion = "Piedra";

            jg2.Nombre = "Yuri";
            jg2.Eleccion = "piEDra";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("EMPATE", result);
        }
        [Test]
        public void Test14()
        {
            string result = "";
            jg1.Nombre = "Rodrigo";
            jg1.Eleccion = "Tijera";

            jg2.Nombre = "María";
            jg2.Eleccion = "Papel";
            result = juego.GetGanador(jg1, jg2);

            Assert.AreEqual("Rodrigo", result);
        }
    }
}
