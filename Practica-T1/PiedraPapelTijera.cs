﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_T1
{
    public class PiedraPapelTijera
    {
        private Jugador jugador1 = new Jugador();
        private Jugador jugador2 = new Jugador();


        public string GetGanador(Jugador jg1, Jugador jg2)
        {
            
            jg1.Eleccion = jg1.Eleccion.ToUpper();
            jg2.Eleccion = jg2.Eleccion.ToUpper();
            if ((jg1.Eleccion == "TIJERA" && jg2.Eleccion == "PAPEL") || (jg1.Eleccion == "PIEDRA" && jg2.Eleccion == "TIJERA") || (jg1.Eleccion == "PAPEL" && jg2.Eleccion == "PIEDRA"))
                return jg1.Nombre;
            else if (jg1.Eleccion == jg2.Eleccion)
                return "EMPATE";
            else
                return jg2.Nombre;
        }

    }
}
