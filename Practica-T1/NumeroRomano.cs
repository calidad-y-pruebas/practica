﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_T1
{
    public class NumeroRomano
    {
        public string GetNumeroRomano(int num)
        {
            string romano = "";
            int Miles, Resto, Cen, Dec, Uni, N;

            N = num;
            Miles = N / 1000;
            Resto = N % 1000;
            Cen = Resto / 100;
            Resto = Resto % 100;
            Dec = Resto / 10;
            Resto = Resto % 10;
            Uni = Resto;
            
            switch (Miles)
            {
                case 1: return "M";
            }
            switch (Cen)
            {
                case 1: romano += "C";  break;
                case 2: romano += "CC"; break;
                case 3: romano += "CCC";break;
                case 4: romano += "CD";break;
                case 5: romano += "D"; break;
                case 6: romano += "DC";break;
                case 7: romano += "DCC";break;
                case 8: romano += "DCCC";break;
                case 9: romano += "CM";break;
            }
            switch (Dec)
            {
                case 1:
                    romano += "X";break;
                case 2:
                    romano += "XX";break;
                case 3:
                    romano += "XXX";break;
                case 4:
                    romano += "XL";break;
                case 5:
                    romano += "L";break;
                case 6:
                    romano += "LX";break;
                case 7:
                    romano += "LXX";break;
                case 8:
                    romano += "LXXX";break;
                case 9:
                    romano += "XC"; break;
            }
            switch (Uni)
            {
                case 1: romano += "I";break;
                case 2: romano += "II";break;
                case 3: romano += "III";break;
                case 4: romano += "IV"; break;
                case 5: romano += "V";break;
                case 6: romano += "VI";break;
                case 7: romano += "VII";break;
                case 8: romano += "VIII";break;
                case 9: romano += "IX"; break;
            }
            return romano;
        }
    }
}
